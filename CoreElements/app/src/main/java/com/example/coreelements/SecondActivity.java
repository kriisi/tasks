package com.example.coreelements;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (getIntent().hasExtra("org.packagename.coreElemens.something")) {
            TextView tv = (TextView)findViewById(R.id.textView);
            String text = getIntent().getExtras().getString("org.packagename.coreElemens.something");
            tv.setText(text);
        }
    }
}
