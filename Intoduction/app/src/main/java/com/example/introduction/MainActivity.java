package com.example.introduction;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //added code
        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText firstNumEditText = (EditText) findViewById(R.id.firstNumEditText);
                EditText secondNumEditText = (EditText) findViewById(R.id.secondNumber);
                TextView resultTextView = (TextView) findViewById(R.id.resultText);


                int num1 = checkNull(firstNumEditText.getText().toString());
                int num2 = checkNull(secondNumEditText.getText().toString());
                int result = num1 + num2;
                resultTextView.setText(result + "");

            }

            //added the null check
            int checkNull(String num) {

                    if (num.equals(""))
                        return 0;
                    else
                        return Integer.parseInt(num);

            }
        });
    }
}
