Project app

--- Motivation ---
This Project app was created mainly for the Software Development Skills: Mobile Course (SDS:Mobile). The idea of the app is to show lists about the events and sights in Oulu area. This application might be improved at some point, but as far as SDS:Mobile goes, this will be the final product for it.


--- Build Status ---
Build is currently 1.0. 
This app will not be updated further under this name or for this course.


--- Tech/Framework Used ---
Built with Android Studio 4.0
Used Java


--- How to Use ---
The application has 3 different views: Home, Event List and Sight List. These can be toggled from the menu, which is found at the top left corner, left side of the "Project" text. This navigation menu is available all the time.

HOME
- Shows only welcoming text.

EVENT LIST 	
- Shows list of different events and the following the name, the date and the venue of the event and shows an Euro sign if the event has costs to participate in it.
- Clicking an event, the app will change the view and show more details about the chosen event. There is also a button to go buy tickets for the event if they are required to participate the event. You may go back to Event List by using the phones own back button.

SIGHT LIST	
- Shows the following information of the sights: the name, the type, if the sight is open and an Euro sight which indicates if the sight has any costs to visit.
- Clicking a sight, the app will change the view and show more details about the chosen sight. There is also a button to go buy tickets for the sight if they are provided in online. You may go back to Sight List by using the phones own back button.


--- Credits ---
This app was created by BitBucket user: Kriisi