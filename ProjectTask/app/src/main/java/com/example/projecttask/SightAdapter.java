package com.example.projecttask;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SightAdapter extends RecyclerView.Adapter<SightAdapter.MyViewHolder>{

    //main variables
    LayoutInflater layoutInflater;
    List<String> nameData; //title/sight name
    List<String> typeData; //type of the sight
    List<Boolean> hasOpenData; //in case sight is open or not (example museum has opening hours, while nature sights are open 24/7)
    List<Boolean> isOpen; //shows if the sight is open now or closed
    List<Boolean> euroSign; //for toggling on/off euro sign in case event has entrance fee
    List<Integer> imgData; //for image
    List<String> ticketUrls;

    final String open = "Open", close = "Closed"; //Strings for open or close


    public SightAdapter(Context ct, List<String> sightName, List<String> sightType, List<Boolean> hasOpeningHours, List<Boolean> isOpen, List<Boolean> hasFee, List<Integer> sightImg, List<String> tickets){
        this.layoutInflater = LayoutInflater.from(ct);
        this.nameData = sightName;
        this.typeData = sightType;
        this.hasOpenData = hasOpeningHours;
        this.isOpen = isOpen;
        this.euroSign = hasFee;
        this.imgData = sightImg;
        this.ticketUrls = tickets;
    }

    @NonNull
    @Override
    public SightAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.sights_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SightAdapter.MyViewHolder holder, int position) {

        //bind the textview with data received

        String name = nameData.get(position);
        String type = typeData.get(position);
        boolean openData = hasOpenData.get(position);
        boolean openNow = isOpen.get(position);
        boolean hasFee = euroSign.get(position);
        int image = imgData.get(position);

        holder.sightName.setText(name);
        holder.sightType.setText(type);
        holder.sightImg.setImageResource(image);

        //changing the euroSign invisible if there is no fee.
        if (!hasFee){
            holder.sightFee.setVisibility(View.INVISIBLE);
        }

        //let's check if the sight has any opening hours
        if (openData){
            //if having opening hours, checking if the sight is now open or closed
            if (openNow)
                holder.sightOpen.setText(open);
            else
                holder.sightOpen.setText(close);

        } else
            holder.sightOpen.setVisibility(View.INVISIBLE);

    }

    @Override
    public int getItemCount() {
        return nameData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView sightName, sightType, sightOpen;
        ImageView sightImg, sightFee;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), DetailsActivity.class);
                    intent.putExtra("Title", nameData.get(getAdapterPosition()));
                    intent.putExtra("Type", typeData.get(getAdapterPosition()));
                    intent.putExtra("OpenData", hasOpenData.get(getAdapterPosition()));
                    intent.putExtra("Fee", euroSign.get(getAdapterPosition()));
                    intent.putExtra("Img", imgData.get(getAdapterPosition()));
                    intent.putExtra("Tickets", ticketUrls.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);

                }
            });

            sightName = itemView.findViewById(R.id.sightNameView);
            sightType = itemView.findViewById(R.id.sightTypeView);
            sightOpen = itemView.findViewById(R.id.sightOpenView);
            sightFee = (ImageView) itemView.findViewById(R.id.sightHasPriceView);
            sightImg = (ImageView) itemView.findViewById(R.id.sightImg);
        }
    }
}