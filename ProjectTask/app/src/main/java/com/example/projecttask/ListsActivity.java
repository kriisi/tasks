package com.example.projecttask;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class ListsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    int listNumber;

    //Variables for both event and sight lists
    RecyclerView recyclerView;
    EventAdapter eventAdapter;
    SightAdapter sightAdapter;

    //event Arrays
    ArrayList<String> eNames, eDates, eVenues;
    ArrayList<Boolean> eFee;
    ArrayList<String> eTickets;
    ArrayList<Integer> eImg;

    //sight Arrays
    ArrayList<String> sName;
    ArrayList<Boolean> hasData, isOpen, sFee;
    ArrayList<Integer> sTypeCode;
    ArrayList<String> sType;
    ArrayList<String> sTickets;
    ArrayList<Integer> sImg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        //menu and toolbar hooks
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        //custom toolbar
        setSupportActionBar(toolbar);

        //Menu
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        //--- From here starts Lists pages of the application ---//

        /*Guides used as reference to create RecyclerView and related adapters:
            https://www.youtube.com/watch?v=PEofA7Z8e_Q
            https://youtu.be/18VcnYN5_LM
        */

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Intent in = getIntent();
        listNumber = in.getIntExtra("menu_LIST", -1);

        if (listNumber == 1) {
            setEvents();
            eventAdapter = new EventAdapter(this, eNames, eDates, eVenues, eFee, eImg, eTickets);
            recyclerView.setAdapter(eventAdapter);
            navigationView.setCheckedItem(R.id.eventList);
        }
        else if (listNumber == 2){
            setSights();
            sightAdapter = new SightAdapter(this,sName,sType,hasData,isOpen,sFee,sImg, sTickets);
            recyclerView.setAdapter(sightAdapter);
            navigationView.setCheckedItem(R.id.sightList);
        }


    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    private void setEvents() {

        eNames = new ArrayList<>();
        eNames.add("Event Name");
        eNames.add("Movie");
        eNames.add("Theater Show");

        eDates = new ArrayList<>();
        eDates.add("Event Date");
        eDates.add("Today, starts 19.30");
        eDates.add("Tomorrow at 18.00");

        eVenues = new ArrayList<>();
        eVenues.add("Venue");
        eVenues.add("Finnkino");
        eVenues.add("Oulu City Theater");

        eFee = new ArrayList<>();
        eFee.add(true);
        eFee.add(true);
        eFee.add(false);

        eTickets = new ArrayList<>();
        eTickets.add("https://www.ticketmaster.fi/");
        eTickets.add("https://www.finnkino.fi/");
        eTickets.add("https://teatteri.ouka.fi");

        eImg = new ArrayList<>();
        eImg.add(R.drawable.ic_launcher_background);
        eImg.add(R.drawable.finnkino);
        eImg.add(R.drawable.ouluteatteri);
    }

    private void setSights(){
        sName = new ArrayList<>();
        sName.add("Toripolliisi");
        sName.add("Tietomaa");
        sName.add("Turkansaari");
        sName.add("Salamapaja");

        //Nature and Parks (1), Museum and Galleries (2), Buildings and Public Art (3), Other sights (4);
        sTypeCode = new ArrayList<>();
        sTypeCode.add(3);
        sTypeCode.add(2);
        sTypeCode.add(1);
        sTypeCode.add(4);

        //does the sight have opening hours data
        hasData = new ArrayList<>();
        hasData.add(false);
        hasData.add(true);
        hasData.add(true);
        hasData.add(true);

        //is the sight open now?
        isOpen = new ArrayList<>();
        isOpen.add(true);
        isOpen.add(true);
        isOpen.add(false);
        isOpen.add(false);

        sFee = new ArrayList<>();
        sFee.add(false);
        sFee.add(true);
        sFee.add(false);
        sFee.add(true);

        sTickets = new ArrayList<>();
        sTickets.add("https://www.google.com/");
        sTickets.add("https://osta.museokortti.fi/fi");
        sTickets.add("https://www.google.com/");
        sTickets.add("https://salamapaja.fi/tours/tours_suo/aurora_pt_suo/");

        sImg = new ArrayList<>();
        sImg.add(R.drawable.toripolliisi);
        sImg.add(R.drawable.tietomaa);
        sImg.add(R.drawable.turkansaari);
        sImg.add(R.drawable.salamapaja);

        //creating the strings for sight types, since there are only certain ones.
        sType = new ArrayList<>();
        int i = 0;
        while (i<sTypeCode.size()){
            switch (sTypeCode.get(i)){

                case 1:
                    sType.add("Nature and Parks");
                    i++;
                    break;
                case 2:
                    sType.add("Museum and Galleries");
                    i++;
                    break;
                case 3:
                    sType.add("Buildings and Public Art");
                    i++;
                    break;
                default:
                    sType.add("Other sights");
                    i++;
                    break;
            }
        } //while

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent;

        switch (item.getItemId()){
            case R.id.home:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
            case R.id.eventList:
                if (listNumber == 1)
                    break;
                intent = new Intent(getApplicationContext(), ListsActivity.class);
                intent.putExtra("menu_LIST", 1);
                startActivity(intent);
                break;
            case R.id.sightList:
                if (listNumber == 2)
                    break;
                intent = new Intent(getApplicationContext(), ListsActivity.class);
                intent.putExtra("menu_LIST", 2);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }
}