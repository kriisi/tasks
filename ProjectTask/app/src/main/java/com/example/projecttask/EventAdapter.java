package com.example.projecttask;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/*Guides used as reference to create RecyclerView and adapters:
            https://www.youtube.com/watch?v=PEofA7Z8e_Q
            https://youtu.be/18VcnYN5_LM
*/

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder>{

    //main variables
    LayoutInflater layoutInflater;
    List<String> nameData; //title/event name
    List<String> dateData; //event date and time
    List<String> venueData; //event venue
    List<Integer> imgData;
    List<Boolean> euroSign; //for toggling on/off euro sign in case event has entrance fee
    List<String> ticketUrls;



    public EventAdapter(Context ct, List<String> eventName, List<String> eventDate, List<String> eventVenue, List<Boolean> hasFee, List<Integer> eventImg, List<String> tickets){
        this.layoutInflater = LayoutInflater.from(ct);
        this.nameData = eventName;
        this.dateData = eventDate;
        this.venueData = eventVenue;
        this.euroSign = hasFee;
        this.imgData = eventImg;
        this.ticketUrls = tickets;
    }

    @NonNull
    @Override
    public EventAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.events_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventAdapter.MyViewHolder holder, int position) {

        //bind the textview with data received

        String name = nameData.get(position);
        String date = dateData.get(position);
        String venue = venueData.get(position);
        boolean hasFee = euroSign.get(position);
        int image = imgData.get(position);

        holder.eventName.setText(name);
        holder.eventDate.setText(date);
        holder.eventVenue.setText(venue);
        holder.eventImg.setImageResource(image);

        //changing the euroSign invisible if there is no fee.
        if (!hasFee){
            holder.eventFee.setVisibility(View.INVISIBLE);
        }


    }

    @Override
    public int getItemCount() {
        return nameData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView eventName, eventDate, eventVenue;
        ImageView eventImg, eventFee;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(v.getContext(), DetailsActivity.class);
                    intent.putExtra("Title", nameData.get(getAdapterPosition()));
                    intent.putExtra("Type", venueData.get(getAdapterPosition()));
                    intent.putExtra("OpenData", true);
                    intent.putExtra("Date", dateData.get(getAdapterPosition()));
                    intent.putExtra("Fee", euroSign.get(getAdapterPosition()));
                    intent.putExtra("Img", imgData.get(getAdapterPosition()));
                    intent.putExtra("Tickets", ticketUrls.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);

                }
            });

            eventName = itemView.findViewById(R.id.eventNameView);
            eventDate = itemView.findViewById(R.id.eventDateView);
            eventVenue = itemView.findViewById(R.id.venueView);
            eventFee = (ImageView) itemView.findViewById(R.id.eventHasPriceView);
            eventImg = (ImageView) itemView.findViewById(R.id.eventImg);
        }
    }
}


