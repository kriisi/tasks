package com.example.projecttask;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

public class DetailsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    int listNumber;

    TextView title, type, timetable, price, buyTickets;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //menu and toolbar hooks
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        //custom toolbar
        setSupportActionBar(toolbar);

        //Menu
        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        //--- details ---//

        title = findViewById(R.id.nameView);
        type = findViewById(R.id.typeView); //sight type or name of the Venue
        timetable = findViewById(R.id.timetableView);
        price = findViewById(R.id.priceView);
        buyTickets = findViewById(R.id.buyTicketsView);
        image = findViewById(R.id.detailImageView);

        Intent i = getIntent();
        String name = i.getStringExtra("Title");
        String typeOrVenue = i.getStringExtra("Type"); //type or name of the Venue
        final String tickets = i.getStringExtra("Tickets");
        int img = i.getIntExtra("Img", -1);

        title.setText(name);
        type.setText(typeOrVenue);
        image.setImageResource(img);


        boolean hasTimetable = i.getBooleanExtra("OpenData", true);

        if (!hasTimetable)
            timetable.setVisibility(View.INVISIBLE);
        else {
            if (i.hasExtra("Date"))
                timetable.setText(i.getStringExtra("Date"));
        }

        boolean prices = i.getBooleanExtra("Fee", true);

        if (!prices){
            price.setVisibility(View.INVISIBLE);
            buyTickets.setVisibility(View.INVISIBLE);
        } else {
            if (tickets == null){
                buyTickets.setVisibility(View.INVISIBLE);
            } else{
                buyTickets.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri webAddress = Uri.parse(tickets);

                        Intent url = new Intent(Intent.ACTION_VIEW, webAddress);
                        if (url.resolveActivity(getPackageManager()) != null)
                            startActivity(url);
                    }
                });
            }
        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intent;

        switch (item.getItemId()){
                case R.id.home:
                    intent = new Intent(DetailsActivity.this, MainActivity.class);
                    startActivity(intent);
                break;
            case R.id.eventList:
                if (listNumber == 1)
                    break;
                intent = new Intent(getApplicationContext(), ListsActivity.class);
                intent.putExtra("menu_LIST", 1);
                startActivity(intent);
                break;
            case R.id.sightList:
                if (listNumber == 2)
                    break;
                intent = new Intent(getApplicationContext(), ListsActivity.class);
                intent.putExtra("menu_LIST", 2);
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }
}